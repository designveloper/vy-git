# GIT-DSV

## It is a software:
* To keep track of changes 
	> Version Control System (VCS) <br/>
	 Managing source code > Source code management (SCM)
* Distributed version control
	* dif. (teams of) users maintain their own repositories
	* changes are stored as *change sets*
		* track changes, not versions
		* exchange changes between repositories
	* have many working copies w/ dif. change sets
	* no need to communicate w/ central server
	* encourage participation and forking pj (dev work indepently)
	
## Installing Git	
* **Configuring git**
	Modifying config file
	* System: `git config --system`
	* User: `git config --global`
	* Pj: `git config`
* **Help**
	`git help + <command>` <br/>
  	`man git-<command>`

## Getting Started
* **Initializing a repository**: `git init` (must lead to the target dir first)
* Git files are stored in .git folder
* Performing first commit: make changes > add changes > commit (just ask `help`)
* **Commit messages**
	* keep it short (50 characters, optionally followed by a blank line 72-character line)
	* use present tense
	* be clear and descriptive
* View commit log: `git log`
	
## Git Concepts and Architecture
* **Three-trees architecture** 
	* **working** (space): our file here
	* **staging index**: add from working (prepare things...)
	* **repository**: commit from staging or working (w/ commit messages...)
* **Using hash values**: using *checksum* to indentify data (and track changes)
* **Working with the HEAD pointer** *(linked list?)*
	* pointed to the "tip" of current branch
	* last check out
	* point to parents of next commit
	* read file HEAD in .git to know where HEAD is
		
## Making changes to files
* #### Adding files
	`git status`: show dif. between 3 trees
* #### Viewing changes w/ diff: 
	* working: `git diff <file name here>`
	* staging: `git diff --staged <file name here>`
* #### Deleting files 
	`git rm`(instead of `add`, but up-to-date git now can use `add` in this situation so...): can remove file without delete manually
* #### Moving and renaming files
	* Manually: `git add <new-name> <br/> git rm <old-name>`
	* Git: `git mv <old-name> <new-name/new-dir>`
* #### Discard file changes in working: 
	`git checkout <name of file/branch/...>`
	> what it does: go to repository, get the named thing it was given and make the working dir look like that
		
## Using git w/ a real pj
* **Fold long file**: `-` + `shift` + `s` + `return`
	return back: `-s` + `return`
* `git diff --color-words` : color the dif.
* `git commit -a`: add and commit on 1 step (good use in modification)
	> `git commit -am "msg"`
  
## Undoing changes
* **Undoing working dir changes**: Discard file changes in working: `git checkout <name of file/branch/...>`
	> what it does: go to repository, get the named thing it was given and make the working dir look like that <br/>
	use `--` before name (of file) to show that we refer to current (file of) branch
* **Unstaging files**: `git reset HEAD <file-name>`
* **Amending commits**: `git commit --amend`: altering the latest commit
* **Retrieving old versions**: using `checkout`
* **Reverting a commit**: (undo changes of latest commit) `git revert <checksum of commit>`
* **Using reset to undo commits**: undo multiple commits (powerful and dangerous)
	* `--soft`: only move the pointer (repository changed)
	* `--mixed` (default): move pointer and change staging index
	* `--hard`: completely reset all (including working)
* **Removing untracked files**: (the file not added into staging index)
	* `git clean -n`: test mode
	* `git clean -f`: for real this time
		
## Ignoring files
* **Using `.gitignore` files** *(a file we create in pj dir)*
	* it can use `*` `?` `[0-9]` `[aeiou]` and `!`
	* each line reprent each (kind of) file or a whole dir to be ignored by Git
	* comment line begins w/ `#`
	* `*` only applies to file
	* apply to files inside the folder where you put `.gitignore`
* **What to ignore**
	* compiled source code
	* package/compressed file
	* logs and db
	* OS generated files
	* user-uploaded assets (images, pdf, videos,...)
* **Ignoring file globally**: `git config --global core.excludesfile <path to .gitignore_global>`
* **Ignoring tracked files** (from *staging index*): `git rm --cached <file-name>`
* Tracking empty directories: git ignore *empty folder*, it must have a file in it (git only keep track of files)
	> make a tiny file called `.gitkeep` inside that folder
	
## Navigating commit tree
* #### Referecing commits *(tree-ish)*
	* checksum (full or short ver.)
	* HEAD pointer
	* branch ref, tag ref
	* ancestry: 
	`HEAD/checksum/master` + `^`(^^..) / `HEAD~` + `(number of level to go up)`
* #### Exploring tree listings: `ls-tree <tree-ish>`
	* tree: dir
	* blob: anything not a tree
* #### Getting more from the commit log *(check `help`)*
	* `git log --oneline`: show the short checksum and commit msg only
	* `--since/after` and `--until`
	* `--author`
	* `--grep`: search keyword globally 
	* `<checksum>..<checksum>`
	* `<file-name>`: only commit including the file
	* `-p`: show the changes of all the commit
	* `--format` (see `help`)
	* `--online --graph --all --decorate`: nine, compact, and show branches
* #### Viewing commits: 
	> `git show <tree-ish>`
	* can use `--format`.
	* can use for commits, trees, tags,.. (refer `help` for more info)
* Comparing commits 
	* `git diff <checksum> <file-name>`: compare between cur and <checksum>
	* `git diff <checksum1>..<checksum2/HEAD>`: compare between c1 and c2
	* `--stat --summary`
	* `-b` = `--ignore-space-change`
	* `-w` = `--ignore-all-space`
		
## Branching
* #### Viewing and creating branches
	* `git branch`: show tree of branches
	* `git branch <name> <branch-start-point>`: create new branch (HEAD still in cur, not shifted to branch yet)
* #### Switching branches
	* `git checkout <branch-name>`: switch to branch 
* #### Creating and switching branches *(at the same time)*
	* `git checkout -b <name>`
* #### Switching branches w/ uncommitted changes 
	> if there're changes, git will not allow to switch branch b4 changes are saved
* #### Comparing branches
	* `git diff <old-branch-name>..<new-branch-name2>`
* #### Renaming branches
	* `git branch -m <old-name> <new-name>`
* #### Deleting branches 
	> `git branch -d <branch-name>`
	* cannot delete current branch you're on
	* cannot delete branch having some changes not merged yet
		> use: `git branch -D <branch-name>` if you're *realy* sure you want to del it
* **Configuring the cmd prompt to show the branch**

## Merching branches
* #### Merging codes
	* checkout to branch to merge in
	* `git merge <merge-branch-name> -m <msg>`
* #### Using fast-forward vs. true merge
	> fast-forward: no change in master since branching, so it simply moves the HEAD forward
	* `--no-ff`: force git to merge
	* `--ff-only`: only merge when we can do a fast-forward
* #### Merging conflicts
	* git will mark the conflicts on the file while in merging mode
	* Abort merge: `git merge --abort`
	* resolve conflicts manually: git helped us mark, so go in and fix them
	* using merging tools: `git mergetool <name>`
* #### Strategies to reduce merge conflicts
	* keep lines *short*
	* keep *commits* small and focused
	* beware stray edits for *whitespace*
	* merge *often*
	* **track changes on master** (stay in sync w/ it!): merge master to branches

## Stashing changes:
	> stash: a place where we can store changes temporary without having to commit them to repo 
	(the 4th separating area)
* #### Saving changes to stash
	* `git stash save <msg>` 
* #### Viewing stashed changes
	* `git stash list`(available in all branches)
	* `git stash show <stash-id>`: summary of what is dif. in that stash
		* `-p`: show the full dif.
* #### Retrieving stashed changes: 
	* `git stash pop <id>`: pop out (and delete in stash)
	* `git stash apply <id>`: still stay in stash
	* if not specify id, it will choose the 0 stash
* #### Deleting stashed changes 
	* one stash: `git stash drop <id>`
	* all stashes: `git stash clear`
		
## Remotes *(need careful reference here)*
	> Local repo has the branch origin to stay in sync w/ remote repo
	our local master branch is a branch from origin master branch
* #### Adding a remote repo *(refer to GitHub introduction)*
	* Add: `git remote add <remote-repo-name> <url>`
	* we can choose rrn as we want, default by gitHub is `origin`
	* remove: `git remote rm <remote-repo-name>`
* #### Creating a remote branch
	* `git push -u <r-r-n> <branch-to-push>`
	* `git branch -r`: show the remote branch
	* `git branch -a`: show all branches (remote and local)
* #### Cloing a remote repo 
	* `git clone <url> <name>*
* #### Tracking remote branches
	* `git branch --set-upstream-to/-u <branch-to-track> <tracking-branch>`
	* `git config branch.<branch-name>.remote <remote-name>`
	* `git config branch.<branch-name>.merge <remote-branch>`
	* reference `git branch` for more info
* #### Pushing changes to a remote repo
	* `git push (<remote-name> <branch-name>)`
* #### Fetching changes from a remote repo
	> `git fetch <remote-name>`
	* `origin/master` sync, but `master` dont
	* fetch b4 work/push
	* fetch *often*
* #### Merging in fetched changes 
	* `git merge origin/master`
	* `git pull` = fetch + merge
* #### Checking out remote branches
	* `git branch <new-branch> <remote-branch>`: create new branch that tracks the remote branch
	* `git checkout -b <new-branch> <remote-branch>`: see above
* #### Pushing to an updated remote branch
	> fetch and merge b4 push
* #### Deleting a remote branch 
	* `git push origin :<branch-name>`
		> actually, `git push origin <branch-name>` is short for `git push origin <branch-name>:<branch-name>`
		<br/> which means, push <b-n> to a branch named <b-n>
		so the delete command above means push nothing to <b-n>
	* `git push --delete <branch-name>`
* #### Enabling collaboration
	* if we're admin, simply add another user into the pj (to grant them write access)
	* if we're not, (first check network and issues) using "pull request" and wait for the pj's admin to accept and merge your part into their pj
	
## Tools and Next Steps:
* #### Setting up aliases from normal command
	* `git config --global alias.<alias-cmd> "<normal-cmd>"` (quote is used for cmd has space)
* #### Using SSH keys for remote login
 	* refer to `git:help` (rmb to change the pj from http to ssh)
* #### Git hosting
	* GitHub
	* Bitbucket
	* Gitorious
	* Gitolite (self-hosting)